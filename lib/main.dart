import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Row & Column',
      home: Scaffold(
        appBar: AppBar(
          title: Text("Row & Column"),
        ),
        body: Container(
          padding: EdgeInsets.only(top: 20, left: 20),
          child: Row(
            children: [
              CircleAvatar(
                radius: 60,
                backgroundImage: AssetImage('assets/images/cat.jpg'),
              ),
              Container(
                margin: EdgeInsets.only(
                  left: 15,
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Ratchanon',
                      style:
                          TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                    ),
                    Text(
                      'Back-end Devloper',
                      style:
                          TextStyle(fontSize: 20),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
